jQuery(function($) {
	$(window).ready(function() {
		adjustSizes()


		$('.hero h1').flowtype({
			minFont   : 40,
			maxFont   : 50,
			fontRatio : 30
		})

		$('.hero p').flowtype({
			minFont: 14,
			maxFont: 18,
			fontRatio : 30
		})


	})

	$(window).resize(function() {
		adjustSizes()
	})

	function adjustSizes()
	{	
		var windowHeight = $(window).height()

		$('.hero').height(windowHeight - 100)

		$('.customers').height((windowHeight - 100) * 0.8)

		$('.reviews').height((windowHeight - 100) * 0.2)

		$('.process').height(windowHeight - 100)

		$('.hero-wrapper').css({
			marginTop : ((windowHeight - 100) / 2) - ($('.hero-wrapper').height() / 2)
		})
	}
})